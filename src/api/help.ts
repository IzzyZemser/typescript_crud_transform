import { type } from "os";

export const getEnv = (name: string): string => {
    const val = process.env[name];
    if(!val){
        console.log("*******",name)
        throw new Error("Invalid env const provided");
    } 
    return val;
}

export const validateParams = (page:string|number, items:string|number):{pageNum:number, itemsNum:number} => {
    let pageNum = typeof page === 'string' ? Number(page)  : page;
    let itemsNum = typeof items === 'string' ? Number(items)  : items;
    return {pageNum, itemsNum}
}