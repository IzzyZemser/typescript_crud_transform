import {Request, Response, NextFunction} from 'express'
// const handler: (x: number, y: number) => number;
const handler =  (fn: (req: Request, res: Response, next: NextFunction) => any)=> (req: Request, res: Response, next: NextFunction) => {
  fn(req, res, next).catch(next);
  // .catch((err)=> next(err))
};
export default handler