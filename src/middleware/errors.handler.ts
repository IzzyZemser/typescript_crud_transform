
// const { NODE_ENV } = process.env;
import {Request, Response, NextFunction} from 'express'
import {getEnv} from '../api/help'

const NODE_ENV = getEnv("NODE_ENV");

// export const error_handler =  (err:error, req:Request, res:Response, next:NextFunction) => {
//     console.log(err);
//     next(err)
// }
// export const error_handler2 =  (err:error, req:Request, res:Response, next:NextFunction) => {
//     if(NODE_ENV !== 'production')res.status(500).json({status:err.message,stack:err.stack});
//     else res.status(500).json({status:'internal server error...'});
// }
export const not_found =  (req:Request, res:Response,) => {
    res.status(404).json({status:`url: ${req.url} not found...`});
}


