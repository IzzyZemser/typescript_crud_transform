/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper";
import express from 'express';
import getConnection from '../../db/sql.connection'
import {Request, Response, NextFunction} from 'express'
import {validateParams} from '../../api/help'
// import {userSchema, usersSchema} from '../../schemas.mjs'

const router = express.Router();

// parse json req.body on post routes
router.use(express.json())

router.get( "/try", (req: Request, res: Response) => {
    
   res.status(200).send("hi");
 });

//Get all USERs
router.get( "/",raw(async (req: Request, res: Response) => {
  const [rows, fields] =  await getConnection().query('SELECT * FROM playground.data;')
  res.status(200).json(rows);
})
);

router.get('/paginate/:page?/:items?', raw( async(req: Request, res: Response)=> {
  let { page = 0 ,items= 10 } = req.params;
  let { pageNum, itemsNum}= validateParams(page, items)
  const [rows] =  await getConnection().query(`SELECT * FROM data LIMIT ${itemsNum} OFFSET ${pageNum * itemsNum}`)
  res.status(200).json(rows);
}))

// // GETS A SINGLE USER
router.get("/:id",raw(async (req: Request, res: Response) => {
  const [rows] =  await getConnection().query(`SELECT * FROM playground.data WHERE id=${req.params.id}`)
  if(!rows.toString()){
    return res.status(404).send("User not found");
  }else{
    res.status(200).json(rows);
  }}));

// //Add single user
router.post("/", raw(async (req: Request, res: Response) => {
    let { first_name ,last_name, email, country} = req.body;
    const [rows] =  await getConnection().query(`INSERT INTO data (first_name, last_name, email, country) values ("${first_name}", "${last_name}","${email}", "${country}");`)
    res.status(200).json(rows);
}));

// Adds Multiple users
router.post("/add-many", raw(async (req: Request, res: Response) => {
  const valString = req.body.reduce((acc: string, cur: any) => {
    if(acc){
      acc += " , "
    }
    return acc + ` ("${cur.first_name}", "${cur.last_name}","${cur.email}", "${cur.country}")`
  }, "");

  const [rows] =  await getConnection().query(`INSERT INTO data (first_name, last_name, email, country) values ${valString};`)
    res.status(200).json(rows);
}));

// // UPDATES A SINGLE USER
router.put("/:id",raw(async (req:Request, res: Response) => {
    let valString:string = "";
    for(let key of Object.keys(req.body)){
      if(valString){
        valString += " , ";
      }
      valString += ` ${key}="${req.body[key]}"`;
    }
    const [rows] =  await getConnection().query(`UPDATE data SET ${valString} WHERE id=${req.params.id};
    `)
    res.status(200).json(rows);
})
);


// // DELETES A USER
router.delete("/:id",raw(async (req: Request, res: Response) => {
  const [rows] =  await getConnection().query(`DELETE FROM data WHERE id=${req.params.id};`)
  res.status(200).json(rows);
 
})
);

export default router;
