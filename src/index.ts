import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import dotenv from 'dotenv'
import getConnection from './db/sql.connection'
import user_router from './modules/user/user.router';
import {getEnv} from './api/help'

dotenv.config({ path: __dirname+'/.env'  });

console.log(process.env.PORT)

const PORT = getEnv("PORT")
const HOST = getEnv("HOST")
console.log(PORT, HOST)
const app = express();

// middleware
app.use(cors());
app.use(morgan('dev'))

app.set("trust proxy", 1) // trust first proxy
// routing
// app.use('/api/stories', story_router);
app.use('/api/users', user_router);


//when no routes were matched...
app.use('*', (req,res)=>{
    res.send("NOT FOUND")
})

;(async ()=> {
  //connect to mongo db
  getConnection();
  await app.listen(PORT,() => {
    console.log(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`); 
  });
   
})().catch(console.log)