import mysql, {Connection} from "mysql2/promise";
// import dotenv from 'dotenv'
import {getEnv} from '../api/help'

// dotenv.config({ path: __dirname+'/../../.env'  });

// import {config} from '../ormconfig';
// import { ConnectionOptions } from 'typeorm';

let connection: Connection;

let getConnection = () => {
  if(connection){
    return connection;
  }
  (async ()  => {
    connection = await mysql.createConnection({
    host: getEnv("DB_HOST"),
    port: Number( getEnv("DB_PORT")),
    database: getEnv("DB_NAME"),
    user: getEnv("DB_USER_NAME"),
    password: getEnv("DB_USER_PASSWORD"),
  });
  
  await connection.connect();
  console.log("Connected to MySQL DB");
  
})();
return connection
}


// ;(async ()  => {
//     connection = await mysql.createConnection({
//     host: getEnv("DB_HOST"),
//     port: Number( getEnv("DB_PORT")),
//     database: getEnv("DB_NAME"),
//     user: getEnv("DB_USER_NAME"),
//     password: getEnv("DB_USER_PASSWORD"),
//   });
  
//   await connection.connect();
//   console.log("Connected to MySQL DB");
  
// })();
// let connection = await mysql.createConnection(config);
export default getConnection;
